import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Car } from '../../models/car.model';

@Component({
  selector: 'app-car-table',
  templateUrl: './car-table.component.html',
  styleUrls: ['./car-table.component.css']
})
export class CarTableComponent implements OnInit {
  @Input() cars: Car[];
  @Input() editCarId: number;
  @Input() sortColName = '';
  @Output() editCar = new EventEmitter<number>();
  @Output() saveCar = new EventEmitter<Car>();
  @Output() cancelCar = new EventEmitter<void>();
  @Output() sortCars = new EventEmitter<string>();
  @Output() deleteCar = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  doSort(colName: string) {
    this.sortCars.emit(colName);
  }

  doSaveCar(car: Car) {
    this.saveCar.emit(car);
  }

  doDeleteCar(id: number) {
    this.deleteCar.emit(id);
  }
}
