import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';

import { Car } from '../../models/car.model';
import { CarsService } from '../../services/cars.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {
  headerText = 'Car Tool';

  cars: Car[] = [];

  sortColumnName = '';
  sortAsc = true;
  editCarId = -1;

  constructor(private carsService: CarsService) {}

  ngOnInit(): void {
    this.refreshCars();
  }

  doAddCar(car: Car): void {
    this.refreshCars(this.carsService.append(car));
  }

  doDeleteCar(id: number): void {
    this.refreshCars(this.carsService.remove(id));
  }

  doSaveCar(editedCar: Car) {
    this.refreshCars(this.carsService.replace(editedCar));
  }

  doEditCar(carId: number) {
    this.editCarId = carId;
  }

  doCancelEdit() {
    this.editCarId = -1;
  }

  doSortColumn(colName: string) {
    if (this.sortColumnName === colName) {
      this.sortAsc = !this.sortAsc;
    } else {
      this.sortColumnName = colName;
      this.sortAsc = true;
    }
    this.refreshCars();
  }

  refreshCars(o: Observable<any> = null) {
    (!o ? of([]) : o)
      .pipe(
        switchMap(() =>
          this.carsService.all(
            this.sortColumnName,
            this.sortAsc ? 'asc' : 'desc'
          )
        )
      )
      .subscribe({
        next: cars => {
          this.cars = cars;
          this.doCancelEdit();
        },
        error: err => console.log(err)
      });
  }
}
