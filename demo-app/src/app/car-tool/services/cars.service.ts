import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Car } from '../models/car.model';

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  cars: Car[] = [];

  constructor(private httpClient: HttpClient) {}

  all(sortCol = '', sortOrder: string) {
    // return this.cars.concat();
    if (sortCol.length > 0) {
      return this.httpClient.get<Car[]>(
        `http://localhost:4250/cars?_sort=${encodeURIComponent(
          sortCol
        )}&_order=${encodeURIComponent(sortOrder)}`
      );
    } else {
      return this.httpClient.get<Car[]>('http://localhost:4250/cars');
    }
  }

  append(car: Car) {
    return this.httpClient.post<Car>('http://localhost:4250/cars', car);
  }

  replace(car: Car) {
    return this.httpClient.put<Car>(
      `http://localhost:4250/cars/${encodeURIComponent(car.id)}`,
      car
    );
  }

  remove(id: number) {
    return this.httpClient.delete<Car>(
      `http://localhost:4250/cars/${encodeURIComponent(id)}`
    );
  }

  findById(id: number) {
    return this.cars.filter(car => car.id === id);
  }
}
