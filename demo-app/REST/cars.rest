GET http://localhost:4250/cars?_sort=make HTTP/1.1
###
GET http://localhost:4250/cars/1 HTTP/1.1
###
POST http://localhost:4250/cars HTTP/1.1
Content-Type: application/json

{
  "make":"Tesla",
  "model":"S",
  "year":2020,
  "color":"red",
  "price":5000
}
###
DELETE http://localhost:4250/cars/3 HTTP/1.1
